var calendar = new controller();
calendar.init();

function controller(target) {

  var that = this;
  var m_oMonth = new Date();
  m_oMonth.setDate(1);

  this.init = function() {
    that.renderCalendar();
    that.initEvent();
  }

    /* 달력 UI 생성 */
  this.renderCalendar = function() {
    var arrTable = [];

    arrTable.push('<table><colgroup>');
    for(var i=0; i<7; i++) {
      arrTable.push('<col width="100">');
    }
    arrTable.push('</colgroup><thead><tr>');

    var arrWeek = "일월화수목금토".split("");

    for(var i=0, len=arrWeek.length; i<len; i++) {
      var sClass = '';
      sClass += i % 7 == 0 ? 'sun' : '';
      sClass += i % 7 == 6 ? 'sat' : '';
      arrTable.push('<td class="'+sClass+'">' + arrWeek[i] + '</td>');
    }
    arrTable.push('</tr></thead>');
    arrTable.push('<tbody>');

    var oStartDt = new Date(m_oMonth.getTime());
        // 1일에서 1일의 요일을 빼면 그 주 첫번째 날이 나온다.
    oStartDt.setDate(oStartDt.getDate() - oStartDt.getDay());

    for(var i=0; i<100; i++) {
      if(i % 7 == 0) {
        arrTable.push('<tr>');
      }

      var sClass = 'date-cell '
            sClass += m_oMonth.getMonth() != oStartDt.getMonth() ? 'not-this-month ' : '';
      sClass += i % 7 == 0 ? 'sun' : '';
      sClass += i % 7 == 6 ? 'sat' : '';

      var year = oStartDt.getFullYear();
      var month = oStartDt.getMonth()+1;
      var date  = oStartDt.getDate();
      var day   = oStartDt.getDay();
      var key  = year.toString()+month.toString()+date.toString();
      var timestamp = oStartDt.getTime();

      arrTable.push('<td class="'+sClass+'">');
      arrTable.push('<span class="cell-block" ');
      arrTable.push('data-key="'+key+'" ');
      arrTable.push('data-year="'+year+'" ');
      arrTable.push('data-month="'+month+'" ');
      arrTable.push('data-date="'+date+'" ');
      arrTable.push('data-ts="'+timestamp+'" ');
      arrTable.push('data-day="'+day+'"');
      arrTable.push('>' + oStartDt.getDate() + '</span>');
      arrTable.push('</td>');
      oStartDt.setDate(oStartDt.getDate() + 1);

      if(i % 7 == 6) {
        arrTable.push('</tr>');
        if(m_oMonth.getMonth() != oStartDt.getMonth()) {
          break;
        }
      }
    }
    arrTable.push('</tbody></table>');

    $('#calendar').html(arrTable.join(""));

    that.changeMonth();
    that.setOnClickListener();
  }

    /* Next, Prev 버튼 이벤트 */
  this.initEvent = function() {
    $('.calendar-header .btn-left').click(that.onPrevCalendar);
    $('.calendar-header .btn-right').click(that.onNextCalendar);
  }

    /* 이전 달력 */
  this.onPrevCalendar = function() {
    m_oMonth.setMonth(m_oMonth.getMonth() - 1);
    that.renderCalendar();
  }

    /* 다음 달력 */
  this.onNextCalendar = function() {
    m_oMonth.setMonth(m_oMonth.getMonth() + 1);
    that.renderCalendar();
  }

    /* 달력 이동되면 상단에 현재 년 월 다시 표시 */
  this.changeMonth = function() {
    $('.calendar-header .current-date').html(that.getYearMonth(m_oMonth));
  }

    /* 날짜 객체를 년 월 문자 형식으로 변환 */
  this.getYearMonth = function(oDate) {
    return oDate.getFullYear() + '<small>년</small> ' + (oDate.getMonth() + 1) + '<small>월</small>';
  }

  this.setOnClickListener = function(){
    $('#calendar table td .cell-block').on('click', function(){
      if($(this).hasClass('selected')){
        $(this).removeClass('selected');
      }else{
        $(this).addClass('selected');
      }
    });

    $('#calendar table td .cell-block').on('contextmenu', that.onClickContextmenu)
  }

  this.onClickContextmenu = function(e){
    var target = $(e.target);
    if(!target.hasClass('selected')) return false;
    console.log($(e.target).data())
    var clicked = function() { alert('Item clicked!') }

    var items = [
      { title: 'Add Sites', icon: 'ion-plus-round', fn: clicked },
      { title: 'Reset Login', icon: 'ion-person', fn: clicked },
      { title: 'Help', icon: 'ion-help-buoy', fn: clicked },
      { title: 'Disabled', icon: 'ion-minus-circled', fn: clicked, disabled: true },
      { title: 'Invisible', icon: 'ion-eye-disabled', fn: clicked, visible: false },
      { },
      { title: 'Logout', icon: 'ion-log-out', fn: clicked }
    ]

    basicContext.show(items, e)
  }
}
