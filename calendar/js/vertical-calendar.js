
var dayOfWeekKo = ['일','월','화','수','목','금','토'];
var calendar = new controller();
var LConverter = new Lunar();

calendar.init();

function controller(target) {

  var that = this;
  var m_oMonth = new Date();
  m_oMonth.setDate(1);

  this.init = function() {
    that.renderCalendar();
    that.initEvent();
  }

    /* 달력 UI 생성 */
  this.renderCalendar = function() {
    var arrTable = [];

    arrTable.push('<table>');
    arrTable.push('<colgroup>');
    arrTable.push('<col width="25%"/>')
    arrTable.push('<col width="75%"/>')
    arrTable.push('</colgroup>');
    // arrTable.push('<thead><tr>');
    //
    // var arrWeek = "일월화수목금토".split("");
    //
    // for(var i=0, len=arrWeek.length; i<len; i++) {
    //   var sClass = '';
    //   sClass += i % 7 == 0 ? 'sun' : '';
    //   sClass += i % 7 == 6 ? 'sat' : '';
    //   arrTable.push('<td class="'+sClass+'">' + arrWeek[i] + '</td>');
    // }
    // arrTable.push('</tr></thead>');
    arrTable.push('<tbody>');
    var oStartDt = new Date(m_oMonth.getTime());
    var y = oStartDt.getFullYear(), m = oStartDt.getMonth();
    var firstDate = new Date(y, m, 1);
    var lastDate = new Date(y, m + 1, 0);

    var startDate = firstDate.getDate();
    var endDate   = lastDate.getDate();

    for(var i=startDate; i<=endDate; i++) {
      oStartDt.setDate(i);
      var year = oStartDt.getFullYear();
      var month = oStartDt.getMonth()+1;
      var date  = i;
      var dayIdx = oStartDt.getDay();
      var dayKo    = dayOfWeekKo[dayIdx];
      var key  = year.toString()+month.toString()+date.toString();
      var timestamp = oStartDt.getTime();

      var lunarDate = LConverter.solarToLunar(year, month, date).day;
      var dateTxt = (lunarDate<10) ? '0'+lunarDate : lunarDate;
      var water = WaterFormula['서해'][dateTxt];

      var sClass = '';
      sClass += (dayIdx == 0) ? 'sun' : '';
      sClass += (dayIdx == 6) ? 'sat' : '';

      arrTable.push('<tr>');
      arrTable.push('<td class="'+sClass+' text-center">');
      arrTable.push('<div class="date-block" ');
      arrTable.push('data-key="'+key+'" ');
      arrTable.push('data-year="'+year+'" ');
      arrTable.push('data-month="'+month+'" ');
      arrTable.push('data-date="'+date+'" ');
      arrTable.push('data-ts="'+timestamp+'" ');
      arrTable.push('data-day-idx="'+dayIdx+'">');
      arrTable.push(date+' ('+dayKo+')<br>('+water+')');
      arrTable.push('</div>');
      arrTable.push('</td>');
      arrTable.push('<td>');
      arrTable.push('<div id="opt-'+key+'">');
      arrTable.push('<div class="list-group text-left">');
      arrTable.push('<a href="#" class="list-group-item">');
      arrTable.push('Cras justo odio');
      arrTable.push('</a>');
      arrTable.push('<a href="#" class="list-group-item">Dapibus ac facilisis in</a>');
      arrTable.push('</div>');
      arrTable.push('</div>');
      arrTable.push('</td>');
      arrTable.push('</tr>');
    }
    arrTable.push('</tbody></table>');

    $('#calendar').html(arrTable.join(""));

    that.changeMonth();
    that.setOnClickListener();
  }

    /* Next, Prev 버튼 이벤트 */
  this.initEvent = function() {
    $('.calendar-header .btn-left').click(that.onPrevCalendar);
    $('.calendar-header .btn-right').click(that.onNextCalendar);
  }

    /* 이전 달력 */
  this.onPrevCalendar = function() {
    m_oMonth.setMonth(m_oMonth.getMonth() - 1);
    that.renderCalendar();
  }

    /* 다음 달력 */
  this.onNextCalendar = function() {
    m_oMonth.setMonth(m_oMonth.getMonth() + 1);
    that.renderCalendar();
  }

    /* 달력 이동되면 상단에 현재 년 월 다시 표시 */
  this.changeMonth = function() {
    $('.calendar-header .current-date').html(that.getYearMonth(m_oMonth));
  }

    /* 날짜 객체를 년 월 문자 형식으로 변환 */
  this.getYearMonth = function(oDate) {
    return oDate.getFullYear() + '<small>년</small> ' + (oDate.getMonth() + 1) + '<small>월</small>';
  }

  this.setOnClickListener = function(){
    $('#calendar table td .cell-block').on('click', function(){
      if($(this).hasClass('selected')){
        $(this).removeClass('selected');
      }else{
        $(this).addClass('selected');
      }
    });

    $('#calendar table td .cell-block').on('contextmenu', that.onClickContextmenu)
  }

  this.onClickContextmenu = function(e){
    var target = $(e.target);
    if(!target.hasClass('selected')) return false;
    console.log($(e.target).data())
    var clicked = function() { alert('Item clicked!') }

    var items = [
      { title: 'Add Sites', icon: 'ion-plus-round', fn: clicked },
      { title: 'Reset Login', icon: 'ion-person', fn: clicked },
      { title: 'Help', icon: 'ion-help-buoy', fn: clicked },
      { title: 'Disabled', icon: 'ion-minus-circled', fn: clicked, disabled: true },
      { title: 'Invisible', icon: 'ion-eye-disabled', fn: clicked, visible: false },
      { },
      { title: 'Logout', icon: 'ion-log-out', fn: clicked }
    ]

    basicContext.show(items, e)
  }
}
